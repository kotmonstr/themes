<?php

use yii\helpers\Html;
use frontend\assets\AdminAsset;
$this->registerJsFile('/js/youtubeCanal.js', ['depends' => AdminAsset::className()]);

/* @var $this yii\web\View */
/* @var $model common\models\Canal */

$this->title = 'Создать On-line Tv канал';
$this->params['breadcrumbs'][] = ['label' => 'Canals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
