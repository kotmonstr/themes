<?php

use yii\helpers\Html;
use yii\grid\GridView;
use Madcoda\Youtube;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CanalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Canals TV on-line';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box ">
    <div class="box-header with-border">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body" style="display: block;">

        <div class="table-responsive">



    <p>
        <?= Html::a('Создать канал', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [
                'attribute' => 'previw',
                'format' => 'html',
                'value' => function ($dataProvider) {

                    $youtube = new Youtube(array('key' => 'AIzaSyBU4vsvP20CYdFuibdgTMOaZ10vt7JxV5c'));
                    $video = $youtube->getVideoInfo($dataProvider->code);
                    $title = $video->snippet->title;
                    $descr = $video->snippet->description;
                    $imageSrc = $video->snippet->thumbnails->medium->url;

                    return '<img src="'.$imageSrc.'" width="100" height="70">';

                }
            ],
            'name',
            'description',
            'code',

            [
                'attribute' => 'status',
                'format' => 'html',
                'value' => function ($dataProvider) {

                    return $dataProvider->status == 1 ? '<p style="color: green">Aктивен</p>' : '<p style="color: red">Не активен</p>';

                }
            ],

            // 'order',
            // 'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        </div>
    </div>
</div>
