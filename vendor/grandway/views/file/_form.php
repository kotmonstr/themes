<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\assets\AdminAsset;

$arrFilesType = [
        'Книга' => 'Книга',
        'Аудио книга' => 'Аудио книга',
        'Архив' => 'Архив',
        'Плагин'=> 'Плагин',
        'Тема'=> 'Тема',
];
$this->registerJsFile('/js/files-upload.js', ['depends' => AdminAsset::className()]);

?>
<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
            <div id="collapse4" class="body">
                <div class="image-form">


                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id' => 'form-files']]); ?>

                    <?= $form->field($model, 'temp_file')->fileInput()->label('Выберите файл') ?>

                    <?= $form->field($model, 'file_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'file_type')->dropDownList($arrFilesType, ['class' => 'form-control', 'prompt' => '--Select--'])->label('Тип файла', ['class' => 'control-label']); ?>

                    <?= $form->field($model, 'descr')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'image')->textInput(['maxlength' => true, 'style' => 'display:none'])->label('') ?>


                    <?= $form->field($model, 'file')->hiddenInput(['id' => 'file-file' ])->label('') ?>

                    <? if ($model->image) { ?>
                        <img src="<?= $model->image_path . '/' . $model->image ?>" width="200px" alt="">
                    <? } ?>
                    <div class="photo"
                         onchange="startUpload()"><?= $form->field($model, 'file')->fileInput()->label('Картинка', ['class' => 'red-b']) ?></div>
                    <div class="avatar"></div>


                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
