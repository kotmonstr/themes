<div class="container">
    <div class="row">
        <div class="col-lg-7 col-md-7 marg75">
            <h3 class="title"><span>Recent Posts</span></h3>
            <div class="pagination">
                <a href="" id="blog_left" class="prev"></a>
                <a href="" id="blog_right" class="next"></a>
            </div>
            <div class="row marg25">
                <div class="col-lg-12">
                    <div id="blog">
                        <div class="showbiz" data-left="#blog_left" data-right="#blog_right">
                            <div class="overflowholder">
                                <ul>
                                    <li>
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <div class="blog-image"><img src="/GrandWay/assets/images/john.jpg" alt=""></div>
                                                <div class="reveal_opener show_on_hover"><a href="#" class="plus"><span class="openme"><i class="fa fa-plus"></i></span></a></div>
                                            </div>
                                            <div class="detailholder">
                                                <div class="blog-name"><a href="#">Residence Mixte</a></div>
                                                <div class="blog-text">Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat…</div>
                                                <div class="blog-line"><hr>
                                                    <div class="blog-date pull-left">February 9, 2014</div>
                                                    <div class="blog-icons pull-right"><a href="#"><i class="fa fa-comments" style="font-size:14px;"></i> 17</a> <a href="#"><i class="fa fa-heart"></i> 137</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mediaholder ">
                                            <div class="mediaholder_innerwrap">
                                                <div class="blog-image"><img src="/GrandWay/assets/images/business.png" alt=""></div>
                                                <div class="reveal_opener show_on_hover"><a href="#" class="plus"><span class="openme"><i class="fa fa-plus"></i></span></a></div>
                                            </div>
                                            <div class="detailholder">
                                                <div class="blog-name"><a href="#">New Deal Design</a></div>
                                                <div class="blog-text">Asunt in anim uis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat…</div>
                                                <div class="blog-line"><hr>
                                                    <div class="blog-date pull-left">March 12, 2014</div>
                                                    <div class="blog-icons pull-right"><a href="#"><i class="fa fa-comments" style="font-size:14px;"></i> 11</a> <a href="#"><i class="fa fa-heart"></i> 205</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 marg75">
            <h3 class="title"><span>Our Services</span></h3>
            <div class="accordion marg25">
                <section id="one">
                    <span class="active"><a href="#one">Wordpress Theme</a></span>
                    <div style="display:block">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
                <section id="two">
                    <span><a href="#two">Web Design</a></span>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
                <section id="three">
                    <span><a href="#three">PHP Programing</a></span>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
                <section id="four">
                    <span><a href="#four">Photoshop</a></span>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
                <section id="five">
                    <span><a href="#five">HTML 5 / CSS 3</a></span>
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>