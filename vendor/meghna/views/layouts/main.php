<?php


use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainMeghnaAsset;
use common\widgets\Alert;
use app\components\SliderWidget;

MainMeghnaAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/MEGHNA/img/favicon.png" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body id="body">

<?php $this->beginBody() ?>

<?= $this->render('parts-frontend/_preloader'); ?>
<?= $this->render('parts-frontend/_home'); ?>
<?= $this->render('parts-frontend/_fixed_navigation'); ?>
<?= $this->render('parts-frontend/_about'); ?>
<?= $this->render('parts-frontend/_main_features'); ?>
<?= $this->render('parts-frontend/_parallax-section'); ?>
<?= $this->render('parts-frontend/_services'); ?>
<?= $this->render('parts-frontend/_showcase'); ?>
<?= $this->render('parts-frontend/_team_skills'); ?>
<?= $this->render('parts-frontend/_our_team'); ?>
<?= $this->render('parts-frontend/_twitter_feed'); ?>
<?= $this->render('parts-frontend/_pricing'); ?>
<?= $this->render('parts-frontend/_testimonals'); ?>
<?= $this->render('parts-frontend/_blog'); ?>
<?= $this->render('parts-frontend/_contact_us'); ?>

<?//= $this->render('parts-frontend/_mainmenu-wrapper'); ?>

<?//= SliderWidget::widget(['theme'=>'saturn']); ?>

        <?= $content ?>


<?= $this->render('parts-frontend/_footer'); ?>

<a href="javascript:;" id="scrollUp">
    <i class="fa fa-angle-up fa-2x"></i>
</a>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
